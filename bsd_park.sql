/*
Navicat MySQL Data Transfer

Source Server         : localhost_mysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : bsd_park

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-12-29 22:13:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `arenas`
-- ----------------------------
DROP TABLE IF EXISTS `arenas`;
CREATE TABLE `arenas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) DEFAULT NULL,
  `pic` varchar(191) DEFAULT NULL,
  `desc` longtext,
  `operasional_time` longtext,
  `price` decimal(10,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of arenas
-- ----------------------------
INSERT INTO `arenas` VALUES ('2', 'THIS IS ARENA', 'Picture-5e05b84d16af0-5e05b84d16af4.png', '<p>ARENA THIS IS</p>', '<p>20-22</p>', '20000.00', '1', '2019-12-27 14:53:14', '1', '2019-12-27 07:53:14');
INSERT INTO `arenas` VALUES ('3', 'TEST', 'Picture-5e05b8ac66199-5e05b8ac6619c.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque, lacus ut tempor varius, diam urna tempor eros, eu facilisis lacus lorem at ex. Nullam ac luctus orci. Phasellus aliquet a erat vel sollicitudin. In consequat, nunc non dictum varius, orci sapien bibendum enim, at fringilla justo dui at elit. Pellentesque sed ornare ipsum. Aliquam et nisl faucibus, laoreet sapien non, feugiat libero. Donec molestie odio arcu, ut pellentesque turpis feugiat id. Nullam quis leo elementum, cursus leo nec, euismod leo. Phasellus gravida libero vel facilisis finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin non massa turpis. Cras interdum lorem non fringilla rhoncus. Fusce sagittis fringilla accumsan. Vestibulum iaculis malesuada nisl, ac tincidunt dui fermentum viverra. Vestibulum maximus lorem tellus, at dictum risus pulvinar vel.</p><p>Duis sed nisl in orci mattis vestibulum. Donec eleifend nec libero et ullamcorper. Sed sodales erat quis orci pharetra, cursus sodales velit maximus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi semper id metus sodales posuere. Aenean ac tellus ultrices, euismod dolor vel, dignissim velit. Donec ac fringilla metus. Aliquam vel arcu vulputate, auctor sapien id, luctus mauris. Nam a ex pharetra, feugiat elit et, commodo mauris. Ut pellentesque tellus in leo cursus consectetur ut pellentesque ante. In id pharetra lorem, et sagittis neque.</p><p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras non neque sem. Sed interdum enim feugiat est aliquet rutrum. Nullam sit amet diam ullamcorper lorem blandit tristique. Phasellus et interdum neque. Integer scelerisque viverra nisi, quis tincidunt mauris efficitur nec. Sed lacus mauris, ultricies et quam id, suscipit elementum mi. Quisque ac varius nisl.</p><p>Cras malesuada gravida felis at ultricies. Phasellus commodo tristique neque, vel semper justo feugiat vel. Etiam eget felis porttitor, elementum eros a, fringilla nisi. Vivamus laoreet, magna sit amet consequat dapibus, dolor velit faucibus metus, a rhoncus neque sem eget eros. Proin in turpis ex. Nunc quis varius velit, ac dictum nisl. Morbi faucibus dictum vulputate. Vivamus congue orci non dapibus condimentum. Quisque auctor, sem non fringilla laoreet, dolor mi finibus est, nec tristique urna diam sit amet est. Proin pellentesque felis nec risus blandit, in pellentesque dolor gravida. Phasellus in maximus neque. Nunc id diam rutrum, venenatis neque id, fringilla velit. Vestibulum sed velit blandit, dictum mi quis, vestibulum risus. Ut vehicula sagittis nisi ut congue. Maecenas nec euismod ante.</p><p>Aliquam sagittis consectetur odio a posuere. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent ut ipsum neque. Quisque vulputate orci ut sapien sodales vulputate. Sed quis euismod odio. Phasellus faucibus est nec quam mattis volutpat. Maecenas imperdiet gravida arcu, varius luctus sem molestie ac. Sed ullamcorper nunc eu purus mattis lobortis. Vestibulum eu congue ligula, nec hendrerit nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi cursus leo quis nisl lacinia eleifend. Quisque in hendrerit urna. Phasellus id purus hendrerit turpis sagittis faucibus et ac massa. Morbi maximus nisi in leo tincidunt rhoncus eget eu ex. Nulla a fringilla leo.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque, lacus ut tempor varius, diam urna tempor eros, eu facilisis lacus lorem at ex. Nullam ac luctus orci. Phasellus aliquet a erat vel sollicitudin. In consequat, nunc non dictum varius, orci sapien bibendum enim, at fringilla justo dui at elit. Pellentesque sed ornare ipsum. Aliquam et nisl faucibus, laoreet sapien non, feugiat libero. Donec molestie odio arcu, ut pellentesque turpis feugiat id. Nullam quis leo elementum, cursus leo nec, euismod leo. Phasellus gravida libero vel facilisis finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin non massa turpis. Cras interdum lorem non fringilla rhoncus. Fusce sagittis fringilla accumsan. Vestibulum iaculis malesuada nisl, ac tincidunt dui fermentum viverra. Vestibulum maximus lorem tellus, at dictum risus pulvinar vel.</p><p>Duis sed nisl in orci mattis vestibulum. Donec eleifend nec libero et ullamcorper. Sed sodales erat quis orci pharetra, cursus sodales velit maximus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi semper id metus sodales posuere. Aenean ac tellus ultrices, euismod dolor vel, dignissim velit. Donec ac fringilla metus. Aliquam vel arcu vulputate, auctor sapien id, luctus mauris. Nam a ex pharetra, feugiat elit et, commodo mauris. Ut pellentesque tellus in leo cursus consectetur ut pellentesque ante. In id pharetra lorem, et sagittis neque.</p><p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras non neque sem. Sed interdum enim feugiat est aliquet rutrum. Nullam sit amet diam ullamcorper lorem blandit tristique. Phasellus et interdum neque. Integer scelerisque viverra nisi, quis tincidunt mauris efficitur nec. Sed lacus mauris, ultricies et quam id, suscipit elementum mi. Quisque ac varius nisl.</p><p>Cras malesuada gravida felis at ultricies. Phasellus commodo tristique neque, vel semper justo feugiat vel. Etiam eget felis porttitor, elementum eros a, fringilla nisi. Vivamus laoreet, magna sit amet consequat dapibus, dolor velit faucibus metus, a rhoncus neque sem eget eros. Proin in turpis ex. Nunc quis varius velit, ac dictum nisl. Morbi faucibus dictum vulputate. Vivamus congue orci non dapibus condimentum. Quisque auctor, sem non fringilla laoreet, dolor mi finibus est, nec tristique urna diam sit amet est. Proin pellentesque felis nec risus blandit, in pellentesque dolor gravida. Phasellus in maximus neque. Nunc id diam rutrum, venenatis neque id, fringilla velit. Vestibulum sed velit blandit, dictum mi quis, vestibulum risus. Ut vehicula sagittis nisi ut congue. Maecenas nec euismod ante.</p><p>Aliquam sagittis consectetur odio a posuere. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent ut ipsum neque. Quisque vulputate orci ut sapien sodales vulputate. Sed quis euismod odio. Phasellus faucibus est nec quam mattis volutpat. Maecenas imperdiet gravida arcu, varius luctus sem molestie ac. Sed ullamcorper nunc eu purus mattis lobortis. Vestibulum eu congue ligula, nec hendrerit nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi cursus leo quis nisl lacinia eleifend. Quisque in hendrerit urna. Phasellus id purus hendrerit turpis sagittis faucibus et ac massa. Morbi maximus nisi in leo tincidunt rhoncus eget eu ex. Nulla a fringilla leo.</p>', '200000.00', '1', '2019-12-27 07:54:20', null, '2019-12-27 07:54:20');

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` longtext COLLATE utf8mb4_unicode_ci,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('5', 'entah apa yang merasuimu', '<p>apa saja</p>', 'Picture-5e059d5a6b967-5e059d5a6b969.jpeg', 'YToxOntpOjA7YToyOntzOjQ6Im5hbWUiO3M6MTA6IkphdmFzY3JpcHQiO3M6ODoibGFuZ3VhZ2UiO3M6MjoianMiO319', 'Promo', '1', '2019-12-26 20:24:54', '2019-12-27 05:57:46');
INSERT INTO `articles` VALUES ('6', 'lorem ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque, lacus ut tempor varius, diam urna tempor eros, eu facilisis lacus lorem at ex. Nullam ac luctus orci. Phasellus aliquet a erat vel sollicitudin. In consequat, nunc non dictum varius, orci sapien bibendum enim, at fringilla justo dui at elit. Pellentesque sed ornare ipsum. Aliquam et nisl faucibus, laoreet sapien non, feugiat libero. Donec molestie odio arcu, ut pellentesque turpis feugiat id. Nullam quis leo elementum, cursus leo nec, euismod leo. Phasellus gravida libero vel facilisis finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin non massa turpis. Cras interdum lorem non fringilla rhoncus. Fusce sagittis fringilla accumsan. Vestibulum iaculis malesuada nisl, ac tincidunt dui fermentum viverra. Vestibulum maximus lorem tellus, at dictum risus pulvinar vel.</p><p>Duis sed nisl in orci mattis vestibulum. Donec eleifend nec libero et ullamcorper. Sed sodales erat quis orci pharetra, cursus sodales velit maximus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi semper id metus sodales posuere. Aenean ac tellus ultrices, euismod dolor vel, dignissim velit. Donec ac fringilla metus. Aliquam vel arcu vulputate, auctor sapien id, luctus mauris. Nam a ex pharetra, feugiat elit et, commodo mauris. Ut pellentesque tellus in leo cursus consectetur ut pellentesque ante. In id pharetra lorem, et sagittis neque.</p><p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras non neque sem. Sed interdum enim feugiat est aliquet rutrum. Nullam sit amet diam ullamcorper lorem blandit tristique. Phasellus et interdum neque. Integer scelerisque viverra nisi, quis tincidunt mauris efficitur nec. Sed lacus mauris, ultricies et quam id, suscipit elementum mi. Quisque ac varius nisl.</p><p>Cras malesuada gravida felis at ultricies. Phasellus commodo tristique neque, vel semper justo feugiat vel. Etiam eget felis porttitor, elementum eros a, fringilla nisi. Vivamus laoreet, magna sit amet consequat dapibus, dolor velit faucibus metus, a rhoncus neque sem eget eros. Proin in turpis ex. Nunc quis varius velit, ac dictum nisl. Morbi faucibus dictum vulputate. Vivamus congue orci non dapibus condimentum. Quisque auctor, sem non fringilla laoreet, dolor mi finibus est, nec tristique urna diam sit amet est. Proin pellentesque felis nec risus blandit, in pellentesque dolor gravida. Phasellus in maximus neque. Nunc id diam rutrum, venenatis neque id, fringilla velit. Vestibulum sed velit blandit, dictum mi quis, vestibulum risus. Ut vehicula sagittis nisi ut congue. Maecenas nec euismod ante.</p><p>Aliquam sagittis consectetur odio a posuere. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent ut ipsum neque. Quisque vulputate orci ut sapien sodales vulputate. Sed quis euismod odio. Phasellus faucibus est nec quam mattis volutpat. Maecenas imperdiet gravida arcu, varius luctus sem molestie ac. Sed ullamcorper nunc eu purus mattis lobortis. Vestibulum eu congue ligula, nec hendrerit nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi cursus leo quis nisl lacinia eleifend. Quisque in hendrerit urna. Phasellus id purus hendrerit turpis sagittis faucibus et ac massa. Morbi maximus nisi in leo tincidunt rhoncus eget eu ex. Nulla a fringilla leo.</p>', 'Picture-5e059e740f095-5e059e740f098.jpeg', 'YToyOntpOjA7YToyOntzOjQ6Im5hbWUiO3M6NjoiVnVlLmpzIjtzOjg6Imxhbmd1YWdlIjtzOjI6InZ1Ijt9aToxO2E6Mjp7czo0OiJuYW1lIjtzOjExOiJPcGVuIFNvdXJjZSI7czo4OiJsYW5ndWFnZSI7czoyOiJvcyI7fX0=', 'Promo', '1', '2019-12-27 06:02:28', '2019-12-27 06:02:28');

-- ----------------------------
-- Table structure for `charts`
-- ----------------------------
DROP TABLE IF EXISTS `charts`;
CREATE TABLE `charts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `total_price` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of charts
-- ----------------------------
INSERT INTO `charts` VALUES ('1', '1', '1', '1', '2', '200.00', '200.00', '2019-12-29 15:08:45', null, '2019-12-29 15:08:45', null, null);

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of config
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2017_08_21_075229_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2', '2017_08_21_075329_create_users_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_08_21_075429_create_config_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_08_21_075605_create_tasks_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_08_24_053036_create_profiles_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_08_27_104452_create_todos_table', '1');
INSERT INTO `migrations` VALUES ('7', '2019_11_21_144120_create_charts_table', '2');
INSERT INTO `migrations` VALUES ('8', '2019_11_21_144213_create_payments_table', '2');
INSERT INTO `migrations` VALUES ('9', '2019_11_21_144229_create_payment_details_table', '2');
INSERT INTO `migrations` VALUES ('10', '2019_12_24_070144_create_articles_table', '3');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `payments`
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `transaction_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_payment` decimal(8,2) NOT NULL,
  `payment_metod` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of payments
-- ----------------------------

-- ----------------------------
-- Table structure for `payment_details`
-- ----------------------------
DROP TABLE IF EXISTS `payment_details`;
CREATE TABLE `payment_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` decimal(8,2) NOT NULL,
  `product_price_total` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of payment_details
-- ----------------------------

-- ----------------------------
-- Table structure for `plant_events`
-- ----------------------------
DROP TABLE IF EXISTS `plant_events`;
CREATE TABLE `plant_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arena_id` int(11) DEFAULT NULL,
  `name` varchar(191) DEFAULT NULL,
  `organisasi` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `tlp` varchar(191) DEFAULT NULL,
  `tgl_event` date DEFAULT NULL,
  `event_type_id` int(191) DEFAULT NULL,
  `jumlah_peserta` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of plant_events
-- ----------------------------
INSERT INTO `plant_events` VALUES ('1', '1', 'aziz', 'umar', 'email@emmail.com', '038627', '2019-12-29', '1', '12', '2019-12-29 14:52:51', null, '2019-12-29 14:52:51', null);

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) DEFAULT NULL,
  `desc` text,
  `pic` varchar(191) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `discount_price` decimal(10,2) DEFAULT NULL,
  `tag` longtext,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('3', 'helo word', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque, lacus ut tempor varius, diam urna tempor eros, eu facilisis lacus lorem at ex. Nullam ac luctus orci. Phasellus aliquet a erat vel sollicitudin. In consequat, nunc non dictum varius, orci sapien bibendum enim, at fringilla justo dui at elit. Pellentesque sed ornare ipsum. Aliquam et nisl faucibus, laoreet sapien non, feugiat libero. Donec molestie odio arcu, ut pellentesque turpis feugiat id. Nullam quis leo elementum, cursus leo nec, euismod leo. Phasellus gravida libero vel facilisis finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin non massa turpis. Cras interdum lorem non fringilla rhoncus. Fusce sagittis fringilla accumsan. Vestibulum iaculis malesuada nisl, ac tincidunt dui fermentum viverra. Vestibulum maximus lorem tellus, at dictum risus pulvinar vel.</p><p>Duis sed nisl in orci mattis vestibulum. Donec eleifend nec libero et ullamcorper. Sed sodales erat quis orci pharetra, cursus sodales velit maximus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi semper id metus sodales posuere. Aenean ac tellus ultrices, euismod dolor vel, dignissim velit. Donec ac fringilla metus. Aliquam vel arcu vulputate, auctor sapien id, luctus mauris. Nam a ex pharetra, feugiat elit et, commodo mauris. Ut pellentesque tellus in leo cursus consectetur ut pellentesque ante. In id pharetra lorem, et sagittis neque.</p><p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras non neque sem. Sed interdum enim feugiat est aliquet rutrum. Nullam sit amet diam ullamcorper lorem blandit tristique. Phasellus et interdum neque. Integer scelerisque viverra nisi, quis tincidunt mauris efficitur nec. Sed lacus mauris, ultricies et quam id, suscipit elementum mi. Quisque ac varius nisl.</p><p>Cras malesuada gravida felis at ultricies. Phasellus commodo tristique neque, vel semper justo feugiat vel. Etiam eget felis porttitor, elementum eros a, fringilla nisi. Vivamus laoreet, magna sit amet consequat dapibus, dolor velit faucibus metus, a rhoncus neque sem eget eros. Proin in turpis ex. Nunc quis varius velit, ac dictum nisl. Morbi faucibus dictum vulputate. Vivamus congue orci non dapibus condimentum. Quisque auctor, sem non fringilla laoreet, dolor mi finibus est, nec tristique urna diam sit amet est. Proin pellentesque felis nec risus blandit, in pellentesque dolor gravida. Phasellus in maximus neque. Nunc id diam rutrum, venenatis neque id, fringilla velit. Vestibulum sed velit blandit, dictum mi quis, vestibulum risus. Ut vehicula sagittis nisi ut congue. Maecenas nec euismod ante.</p><p>Aliquam sagittis consectetur odio a posuere. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent ut ipsum neque. Quisque vulputate orci ut sapien sodales vulputate. Sed quis euismod odio. Phasellus faucibus est nec quam mattis volutpat. Maecenas imperdiet gravida arcu, varius luctus sem molestie ac. Sed ullamcorper nunc eu purus mattis lobortis. Vestibulum eu congue ligula, nec hendrerit nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi cursus leo quis nisl lacinia eleifend. Quisque in hendrerit urna. Phasellus id purus hendrerit turpis sagittis faucibus et ac massa. Morbi maximus nisi in leo tincidunt rhoncus eget eu ex. Nulla a fringilla leo.</p>', 'Picture-5e05ae161095a-5e05ae161095d.jpeg', '200000.00', '20000.00', 'YTozOntpOjA7YToyOntzOjQ6Im5hbWUiO3M6NjoiVnVlLmpzIjtzOjg6Imxhbmd1YWdlIjtzOjI6InZ1Ijt9aToxO2E6Mjp7czo0OiJuYW1lIjtzOjEwOiJKYXZhc2NyaXB0IjtzOjg6Imxhbmd1YWdlIjtzOjI6ImpzIjt9aToyO2E6Mjp7czo0OiJuYW1lIjtzOjExOiJPcGVuIFNvdXJjZSI7czo4OiJsYW5ndWFnZSI7czoyOiJvcyI7fX0=', '1', '2019-12-27 07:09:10', null, '2019-12-27 07:09:10');

-- ----------------------------
-- Table structure for `profiles`
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_foreign` (`user_id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of profiles
-- ----------------------------
INSERT INTO `profiles` VALUES ('2', '1', 'test', 'test', 'male', '2019-11-22', null, null, null, null, '2019-11-22 11:06:26', '2019-11-22 11:06:32');
INSERT INTO `profiles` VALUES ('7', '6', 'ENTIS', 'purnama', null, null, null, null, null, null, '2019-12-29 05:00:09', '2019-12-29 05:00:09');

-- ----------------------------
-- Table structure for `sliders`
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) DEFAULT NULL,
  `desc` longtext,
  `pic_mobile` varchar(191) DEFAULT NULL,
  `pic_desktop` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sliders
-- ----------------------------
INSERT INTO `sliders` VALUES ('3', 'SLider 1', '<p>ini adalah slider 1</p>', 'Picture-5e05f980d2628-5e05f980d262a.png', 'Picture-5e05f980e8935-5e05f980e8938.png', 'slider1', '1', '2019-12-27 12:30:57', null, '2019-12-27 12:30:57');
INSERT INTO `sliders` VALUES ('4', 'slider 2', '<p>ini adalah slider 2</p>', 'Picture-5e05f9a8328aa-5e05f9a8328ad.jpeg', 'Picture-5e05f9a8ad63f-5e05f9a8ad642.jpeg', 'slider 2', '1', '2019-12-27 12:31:37', null, '2019-12-27 12:31:37');
INSERT INTO `sliders` VALUES ('5', 'slider 33', '<p>ini adalah slider 33</p>', 'Picture-5e05f9c761106-5e05f9c761108.jpeg', 'Picture-5e05f9c7d71e1-5e05f9c7d71e5.jpeg', 'slider 33', '1', '2019-12-27 19:38:12', '1', '2019-12-27 12:38:12');

-- ----------------------------
-- Table structure for `tasks`
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `start_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `progress` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_user_id_foreign` (`user_id`),
  CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of tasks
-- ----------------------------

-- ----------------------------
-- Table structure for `todos`
-- ----------------------------
DROP TABLE IF EXISTS `todos`;
CREATE TABLE `todos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `todo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `todos_user_id_foreign` (`user_id`),
  CONSTRAINT `todos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of todos
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_unique_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_token` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin@admin.com', '$2y$10$UPHiQdCSx6sHS2bLy73D3uyoAcXzN.f4tyS/TMTa3AvMG8o4rF4nu', null, null, 'YFXW0CnPxz', 'activated', 'f2kQlSh1jn', '2019-11-12 15:11:11', '2019-11-12 15:11:11');
INSERT INTO `users` VALUES ('6', 'purnamaaziz08@gmail.com', '$2y$10$ZJbCXG8SG3c9G0RRdUeq4OJRdQ5WCMKN5/1BHVqC41M/LEqDs336C', null, null, '6a7631f0-c694-4922-a240-45295d194a51', 'activated', null, '2019-12-29 05:00:09', '2019-12-29 05:01:33');
