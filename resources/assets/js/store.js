import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use(CKEditor);
// import wysiwyg from "vue-wysiwyg";
// Vue.use(wysiwyg, {}); // config is optional. more below
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'
import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
// import "vue-wysiwyg/dist/vueWysiwyg.css";

import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'


Vue.use(VueQuillEditor, /* { default global options } */)

Vue.use(Loading, {
	// Optional parameters
	  // container: false,
	  canCancel: false,
	  color: '#007bff',
	  loader: 'dots',
	  opacity: 0.5,
	  zIndex: 9999,
  },{
	// slots
  })

  Vue.component('my-currency-input', {
    props: ["value"],
    template: `
        <div>
            <input type="text" class="form-control" v-model="displayValue" @blur="isInputActive = false" @focus="isInputActive = true"/>
        </div>`,
    data: function() {
        return {
            isInputActive: false
        }
    },
    computed: {
        displayValue: {
            get: function() {
                if (this.isInputActive) {
                    // Cursor is inside the input field. unformat display value for user
                    return this.value.toString()
                } else {
                    // User is not modifying now. Format display value for user interface
                    return "IDR " + this.value.toString().replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1\.")
                }
            },
            set: function(modifiedValue) {
                // Recalculate value after ignoring "$" and "," in user input
                let newValue = parseFloat(modifiedValue.replace(/[^\d\.]/g, ""))
                // Ensure that it is not NaN
                if (isNaN(newValue)) {
                    newValue = 0
                }
                // Note: we cannot set this.value as it is a "prop". It needs to be passed to parent component
                // $emit the event so that parent component gets it
                this.$emit('input', newValue)
            }
        }
    }
});
const store = new Vuex.Store({
	state: {
		auth: {
			first_name: '',
			last_name: '',
			email: '',
			avatar: ''
		},
		config: {
			company_name: '',
			contact_person: ''
		}
	},
	mutations: {
		setAuthUserDetail (state, auth) {
        	for (let key of Object.keys(auth)) {
                state.auth[key] = auth[key];
            }
            if ('avatar' in auth)
            	state.auth.avatar = auth.avatar !== null ? auth.avatar : 'avatar.png';
		},
		resetAuthUserDetail (state) {
        	for (let key of Object.keys(state.auth)) {
                state.auth[key] = '';
            }
		},
		setConfig (state, config) {
        	for (let key of Object.keys(config)) {
                state.config[key] = config[key];
            }
		}
	},
	actions: {
		setAuthUserDetail ({ commit }, auth) {
     		commit('setAuthUserDetail',auth);
     	},
     	resetAuthUserDetail ({commit}){
     		commit('resetAuthUserDetail');
     	},
		setConfig ({ commit }, data) {
     		commit('setConfig',data);
     	}
	},
	getters: {
		getAuthUser: (state) => (name) => {
		    return state.auth[name];
		},
		getAuthUserFullName: (state) => {
		    return state.auth['first_name']+' '+state.auth['last_name'];
		},
		getConfig: (state) => (name) => {
		    return state.config[name];
		},
	},
	plugins: [
		createPersistedState({ storage: window.sessionStorage })
	]
});

export default store;