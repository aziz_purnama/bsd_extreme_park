<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1', 'as' => 'v1.'], function () {
    Route::get('/arena/list', 'ArenaController@index');
    Route::get('/arena/list/{id}', 'ArenaController@show');
    Route::get('/product/list', 'ProductController@index');
    Route::get('/product/list/{id}', 'ProductController@show');
    Route::get('/article/list', 'ArticleController@index');
    Route::get('/article/list/{id}', 'ArticleController@show');
    Route::get('/slider/list', 'SliderController@index');
    Route::post('/plant-event', 'PlantEventController@store');
    Route::post('/payment', 'PaymentController@store');

    Route::group(['prefix' => 'auth'], function () {
        Route::post('/login', 'AuthController@authenticate');
        Route::post('/logout', 'AuthController@logout');
        Route::post('/check', 'AuthController@check');
        Route::post('/register', 'AuthController@register');
        Route::get('/activate/{token}', 'AuthController@activate');
        Route::post('/password', 'AuthController@password');
        Route::post('/validate-password-reset', 'AuthController@validatePasswordReset');
        Route::post('/reset', 'AuthController@reset');
        Route::post('/social/token', 'SocialAuthController@getToken');
    });

    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::get('/cart', 'ChartController@index');
        Route::post('/cart', 'ChartController@store');
        Route::patch('/cart/{id}', 'ChartController@update');
        Route::post('/cart/update', 'ChartController@updateDetail');

        Route::get('/auth/user', 'AuthController@getAuthUser');
        Route::post('/task', 'TaskController@store');
        Route::get('/task', 'TaskController@index');
        Route::delete('/task/{id}', 'TaskController@destroy');
        Route::get('/task/{id}', 'TaskController@show');
        Route::patch('/task/{id}', 'TaskController@update');
        Route::post('/task/status', 'TaskController@toggleStatus');

        Route::get('/configuration/fetch', 'ConfigurationController@index');
        Route::post('/configuration', 'ConfigurationController@store');

        Route::get('/user/dashboard', 'UserController@dashboard');
        Route::post('/user', 'UserController@store');
        Route::get('/user', 'UserController@index');
        Route::post('/user/change-password', 'AuthController@changePassword');
        Route::post('/user/update-profile', 'UserController@updateProfile');
        Route::post('/user/update-avatar', 'UserController@updateAvatar');
        Route::post('/user/remove-avatar', 'UserController@removeAvatar');
        Route::delete('/user/{id}', 'UserController@destroy');
        Route::get('/user/{id}', 'UserController@show');
        Route::patch('/user/{id}', 'UserController@update');

        Route::post('todo', 'TodoController@store');
        Route::get('/todo', 'TodoController@index');
        Route::delete('/todo/{id}', 'TodoController@destroy');
        Route::post('/todo/status', 'TodoController@toggleStatus');

        Route::get('/article', 'ArticleController@index');
        Route::post('/article', 'ArticleController@store');
        Route::delete('/article/{id}', 'ArticleController@destroy');
        Route::get('/article/{id}', 'ArticleController@show');
        Route::patch('/article/{id}', 'ArticleController@update');

        Route::get('/product', 'ProductController@index');
        Route::post('/product', 'ProductController@store');
        Route::delete('/product/{id}', 'ProductController@destroy');
        Route::get('/product/{id}', 'ProductController@show');
        Route::patch('/product/{id}', 'ProductController@update');

        Route::get('/arena', 'ArenaController@index');
        Route::post('/arena', 'ArenaController@store');
        Route::delete('/arena/{id}', 'ArenaController@destroy');
        Route::get('/arena/{id}', 'ArenaController@show');
        Route::patch('/arena/{id}', 'ArenaController@update');

        Route::get('/slider', 'SliderController@index');
        Route::post('/slider', 'SliderController@store');
        Route::delete('/slider/{id}', 'SliderController@destroy');
        Route::get('/slider/{id}', 'SliderController@show');
        Route::patch('/slider/{id}', 'SliderController@update');

    });
});
