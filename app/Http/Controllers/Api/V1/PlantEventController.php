<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\PlantEvent\PlantEventRepository;
use Illuminate\Http\Request;

/**
 * PlantEvent Controller.
 */
class PlantEventController extends APIController
{
    /**
     * $plant_event PlantEventRepository.
     *
     * @var object
     */
    protected $plant_event;

    /**
     * @param PlantEventRepository $plant_event
     */
    public function __construct(PlantEventRepository $plant_event)
    {
        $this->plant_event = $plant_event;
    }

    
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $plant_event = $this->plant_event->storePlantEvent($input);

        return $plant_event;
    }

}
