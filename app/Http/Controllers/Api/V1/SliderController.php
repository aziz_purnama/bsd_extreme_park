<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Slider\SliderRepository;
use Illuminate\Http\Request;

/**
 * Slider Controller.
 */
class SliderController extends APIController
{
    /**
     * $slider SliderRepository.
     *
     * @var object
     */
    protected $slider;

    /**
     * @param SliderRepository $slider
     */
    public function __construct(SliderRepository $slider)
    {
        $this->slider = $slider;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $slider =  $this->slider->getAllSlider($request);

        return $slider;
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $slider = $this->slider->storeSlider($input);

        return $slider;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = $this->slider->deleteSlider($id);

        return $response;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $slider = $this->slider->showSlider($id);

        return $slider;
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $slider = $this->slider->updateSlider($input, $id);

        return $slider;
    }

}
