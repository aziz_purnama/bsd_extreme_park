<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Chart\ChartRepository;
use Illuminate\Http\Request;

/**
 * Chart Controller.
 */
class ChartController extends APIController
{
    /**
     * $chart ChartRepository.
     *
     * @var object
     */
    protected $chart;

    /**
     * @param ChartRepository $chart
     */
    public function __construct(ChartRepository $chart)
    {
        $this->chart = $chart;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $chart =  $this->chart->getAllChart($request);

        return $chart;
    }
    
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $chart = $this->chart->storeChart($input);

        return $chart;
    }

      /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $chart = $this->chart->updateChart($input, $id);

        return $chart;
    }

     /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDetail(Request $request)
    {
        $input = $request->all();

        $chart = $this->chart->updateChartDetail($input);

        return $chart;
    }
}
