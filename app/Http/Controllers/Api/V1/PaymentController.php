<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Payment\PaymentRepository;
use Illuminate\Http\Request;

/**
 * Payment Controller.
 */
class PaymentController extends APIController
{
    /**
     * $payment PaymentRepository.
     *
     * @var object
     */
    protected $payment;

    /**
     * @param PaymentRepository $payment
     */
    public function __construct(PaymentRepository $payment)
    {
        $this->payment = $payment;
    }

    
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $payment = $this->payment->storePayment($input);

        return $payment;
    }

}
