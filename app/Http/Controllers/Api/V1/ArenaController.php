<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Arena\ArenaRepository;
use Illuminate\Http\Request;

/**
 * Arena Controller.
 */
class ArenaController extends APIController
{
    /**
     * $arena ArenaRepository.
     *
     * @var object
     */
    protected $arena;

    /**
     * @param ArenaRepository $arena
     */
    public function __construct(ArenaRepository $arena)
    {
        $this->arena = $arena;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $arena =  $this->arena->getAllArena($request);

        return $arena;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $arena =  $this->arena->getAllArenaList($request);

        return $arena;
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $arena = $this->arena->storeArena($input);

        return $arena;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = $this->arena->deleteArena($id);

        return $response;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $arena = $this->arena->showArena($id);

        return $arena;
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $arena = $this->arena->updateArena($input, $id);

        return $arena;
    }

}
