<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;

/**
 * Product Controller.
 */
class ProductController extends APIController
{
    /**
     * $product ProductRepository.
     *
     * @var object
     */
    protected $product;

    /**
     * @param ProductRepository $product
     */
    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $product =  $this->product->getAllProduct($request);

        return $product;
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $product = $this->product->storeProduct($input);

        return $product;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = $this->product->deleteProduct($id);

        return $response;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product = $this->product->showProduct($id);

        return $product;
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $product = $this->product->updateProduct($input, $id);

        return $product;
    }

}
