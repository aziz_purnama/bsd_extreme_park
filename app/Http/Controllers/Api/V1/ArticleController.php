<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\Article\ArticleRepository;
use Illuminate\Http\Request;

/**
 * Article Controller.
 */
class ArticleController extends APIController
{
    /**
     * $article ArticleRepository.
     *
     * @var object
     */
    protected $article;

    /**
     * @param ArticleRepository $article
     */
    public function __construct(ArticleRepository $article)
    {
        $this->articel = $article;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $article =  $this->articel->getAllArticle($request);

        return $article;
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $article = $this->articel->storeArticle($input);

        return $article;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $response = $this->articel->deleteArticle($id);

        return $response;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $article = $this->articel->showArticle($id);

        return $article;
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $article = $this->articel->updateArticle($input, $id);

        return $article;
    }

}
