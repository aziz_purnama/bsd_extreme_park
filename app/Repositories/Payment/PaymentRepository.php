<?php

namespace App\Repositories\Payment;

use App\Repositories\BaseRepository;
use App\Models\Payment\Payment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class PaymentRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Payment::class;

    
    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storePayment($input = null)
    {
        try {

            $validation = Validator::make($input, [
                'name'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $payment = Payment::create([
                'user_id'  => JWTAuth::parseToken()->authenticate()->id,
                'transaction_code'  => request('transaction_code'),
                'transaction_date'  => request('transaction_date'),
                'total_payment'  => request('total_payment'),
                'payment_metod'  => request('payment_metod'),
                'status'  => request('status'),
            ]);

            $payment->save();

            $profile = new Profile();
            $profile->first_name = request('first_name');
            $profile->last_name = request('last_name');
            $profile->date_of_birth = request('date_of_birth');
            $profile->gender = request('gender');
            $user->profile()->save($profile);
            
            return response()->json(['message' => 'Payment added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

}