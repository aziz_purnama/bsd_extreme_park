<?php

namespace App\Repositories\Chart;

use App\Repositories\BaseRepository;
use App\Models\Chart\Chart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class ChartRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Chart::class;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllChart($request)
    {
        try {

            $chart = Chart::select('charts.*','products.title','products.price')->leftJoin('products', 'products.id', '=', 'charts.product_id')->where('charts.status',0);

            $chart->orderBy(request('sortBy'), request('order'));

            return $chart->paginate(request('pageLength'));

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    
    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeChart($input = null)
    {
        try {

            $validation = Validator::make($input, [
                // 'name'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $chart = Chart::create([
                'product_id' => request('product_id'),
                'user_id' => JWTAuth::parseToken()->authenticate()->id,
                'qty' => request('qty'),
                'price' => request('price'),
                'total_price' => request('total_price'),
                'status' => request('status'),
                'created_by' => JWTAuth::parseToken()->authenticate()->id,
            ]);

            $chart->save();

            return response()->json(['message' => 'Chart added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    public function updateChart($input = null, $id = null){
        try {

            $validation = Validator::make($input, [
                // 'qty'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $chart = Chart::whereId($id)->first(); // get arena
            
            $chart->qty = request('qty');
            $chart->price = request('price');
            $chart->total_price = request('total_price');
            $chart->status = request('status');
            $chart->updated_by = JWTAuth::parseToken()->authenticate()->id;
            $chart->save();  // save the arena details


            return response()->json(['message' => 'Chart updated!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    public function updateChartDetail($input = null){
        try {

            $validation = Validator::make($input, [
                // 'qty'    => 'required',
            ]);
            
            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }
            
            Chart::whereIn('id', explode(",",$input['id']))->update(['status' => request('status'), 'updated_by' => JWTAuth::parseToken()->authenticate()->id]);
            
            return response()->json(['message' => 'Chart updated!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

}