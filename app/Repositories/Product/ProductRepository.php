<?php

namespace App\Repositories\Product;

use App\Repositories\BaseRepository;
use App\Models\Product\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class ProductRepository extends BaseRepository
{
    
    protected $product_path = 'images/product/';
    /**
     * Associated Repository Model.
     */
    const MODEL = Product::class;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllProduct($request)
    {
        try {

            $product = Product::select('products.*');

            if ($request->has('title')) {
                $product->where('title', 'like', '%'.request('title').'%');
            }

            if ($request->has('desc')) {
                $product->where('desc', 'like', '%'.request('desc').'%');
            }

            if ($request->has('price')) {
                $product->where('price', 'like', '%'.request('price').'%');
            }

            $product->orderBy(request('sortBy'), request('order'));

            return $product->paginate(request('pageLength'));

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function showProduct($id = null)
    {
        try {

            $product = Product::whereId($id)->first();
            $product->tag = unserialize(base64_decode($product->tag));
            if (!$product) {
                return response()->json(['message' => 'Could not find Product!', 422]);
            }

            return $product;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeProduct($input = null)
    {
        try {

            $validation = Validator::make($input, [
                'pic'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            if (request('pic')) {
                $ext        = getExtentionImages(request('pic'));
                $img_name   = createImageName(uniqid(),$ext[1]);
                $path       = $this->product_path . $img_name;
                $img        = \Image::make(file_get_contents(request('pic')))->save($path);
            }

            $user = Product::create([
                'title'    => request('title'),
                'price'   => request('price'),
                'discount_price'   => request('discount_price'),
                'desc' => request('desc'),
                'pic' => $img_name,
                'tag' => base64_encode(serialize(request('tag'))),
                'created_by' => JWTAuth::parseToken()->authenticate()->id,
            ]);

            $user->save();

            return response()->json(['message' => 'Product added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    public function updateProduct($input = null, $id = null){
        try {

            $validation = Validator::make($input, [
                'pic'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $product = Product::whereId($id)->first(); // get product

            $product->title = request('title');
            $product->price = request('price');
            $product->discount_price = request('discount_price');
            $product->desc = request('desc');
            if (request('is_pic_change')) {
                $ext        = getExtentionImages(request('pic'));
                $img_name   = createImageName(uniqid(),$ext[1]);
                $path       = $this->product_path . $img_name;
                $img        = \Image::make(file_get_contents(request('pic')))->save($path);
                $product->pic = $img_name;
            }
            $product->tag = base64_encode(serialize(request('tag')));
            $product->updated_by = JWTAuth::parseToken()->authenticate()->id;
            $product->save();  // save the product details


            return response()->json(['message' => 'Product updated!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProduct($id = null)
    {
        DB::beginTransaction();

        try {

            $product = Product::find($id);

            if (!$product) {
                return response()->json(['message' => 'Could not find product!'], 422);
            }

            if ($product->delete()) {
                DB::commit();
                $responseArr = [
                    'message' => 'Product has been deleted successfully!',
                ];
            } else {
                DB::rollback();
                $responseArr = [
                    'message' => 'Something went wrong!',
                ];
            }

            return response()->json($responseArr);

        } catch (\Exception $ex) {

            DB::rollback();
            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }
}