<?php

namespace App\Repositories\Article;

use App\Repositories\BaseRepository;
use App\Models\Article\Article;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class ArticleRepository extends BaseRepository
{
    
    protected $article_path = 'images/article/';
    /**
     * Associated Repository Model.
     */
    const MODEL = Article::class;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllArticle($request)
    {
        try {

            $article = Article::select('articles.*');

            if ($request->has('title')) {
                $article->where('title', 'like', '%'.request('title').'%');
            }

            if ($request->has('desc')) {
                $article->where('desc', 'like', '%'.request('desc').'%');
            }

            if ($request->has('category')) {
                $article->where('category', 'like', '%'.request('category').'%');
            }

            $article->orderBy(request('sortBy'), request('order'));

            return $article->paginate(request('pageLength'));

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function showArticle($id = null)
    {
        try {

            $article = Article::whereId($id)->first();
            $article->tag = unserialize(base64_decode($article->tag));
            if (!$article) {
                return response()->json(['message' => 'Could not find Article!', 422]);
            }

            return $article;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeArticle($input = null)
    {
        try {

            $validation = Validator::make($input, [
                'pic'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            if (request('pic')) {
                $ext        = getExtentionImages(request('pic'));
                $img_name   = createImageName(uniqid(),$ext[1]);
                $path       = $this->article_path . $img_name;
                $img_ktp        = \Image::make(file_get_contents(request('pic')))->save($path);
            }

            $user = Article::create([
                'title'    => request('title'),
                'category'   => request('category')['name'],
                'desc' => request('desc'),
                'pic' => $img_name,
                'tag' => base64_encode(serialize(request('tag'))),
                'user_id' => JWTAuth::parseToken()->authenticate()->id,
            ]);

            $user->save();

            return response()->json(['message' => 'Article added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    public function updateArticle($input = null, $id = null){
        try {

            $validation = Validator::make($input, [
                'pic'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $article = Article::whereId($id)->first(); // get article

            $article->title = request('title');
            $article->category = request('category')['name'];
            $article->desc = request('desc');
            if (request('is_pic_change')) {
                $ext        = getExtentionImages(request('pic'));
                $img_name   = createImageName(uniqid(),$ext[1]);
                $path       = $this->article_path . $img_name;
                $img_ktp        = \Image::make(file_get_contents(request('pic')))->save($path);
                $article->pic = $img_name;
            }
            $article->tag = base64_encode(serialize(request('tag')));

            $article->user_id = JWTAuth::parseToken()->authenticate()->id;

            $article->save();  // save the article details


            return response()->json(['message' => 'customer updated!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteArticle($id = null)
    {
        DB::beginTransaction();

        try {

            $article = Article::find($id);

            if (!$article) {
                return response()->json(['message' => 'Could not find article!'], 422);
            }

            if ($article->delete()) {
                DB::commit();
                $responseArr = [
                    'message' => 'Article has been deleted successfully!',
                ];
            } else {
                DB::rollback();
                $responseArr = [
                    'message' => 'Something went wrong!',
                ];
            }

            return response()->json($responseArr);

        } catch (\Exception $ex) {

            DB::rollback();
            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }
}