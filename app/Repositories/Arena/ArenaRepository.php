<?php

namespace App\Repositories\Arena;

use App\Repositories\BaseRepository;
use App\Models\Arena\Arena;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class ArenaRepository extends BaseRepository
{
    
    protected $arena_path = 'images/arena/';
    /**
     * Associated Repository Model.
     */
    const MODEL = Arena::class;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllArena($request)
    {
        try {

            $arena = Arena::select('arenas.*');

            if ($request->has('name')) {
                $arena->where('name', 'like', '%'.request('name').'%');
            }

            if ($request->has('desc')) {
                $arena->where('desc', 'like', '%'.request('desc').'%');
            }

            if ($request->has('price')) {
                $arena->where('price', 'like', '%'.request('price').'%');
            }

            $arena->orderBy(request('sortBy'), request('order'));

            return $arena->paginate(request('pageLength'));

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function showArena($id = null)
    {
        try {

            $arena = Arena::whereId($id)->first();
            if (!$arena) {
                return response()->json(['message' => 'Could not find Arena!', 422]);
            }

            return $arena;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeArena($input = null)
    {
        try {

            $validation = Validator::make($input, [
                'name'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            if (request('pic')) {
                $ext        = getExtentionImages(request('pic'));
                $img_name   = createImageName(uniqid(),$ext[1]);
                $path       = $this->arena_path . $img_name;
                $img        = \Image::make(file_get_contents(request('pic')))->save($path);
            }

            $user = Arena::create([
                'name'    => request('name'),
                'price'   => request('price'),
                'desc' => request('desc'),
                'operasional_time' => request('operasional_time'),
                'pic' => $img_name,
                'created_by' => JWTAuth::parseToken()->authenticate()->id,
            ]);

            $user->save();

            return response()->json(['message' => 'Arena added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    public function updateArena($input = null, $id = null){
        try {

            $validation = Validator::make($input, [
                'name'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $arena = Arena::whereId($id)->first(); // get arena

            $arena->name = request('name');
            $arena->price = request('price');
            $arena->operasional_time = request('operasional_time');
            $arena->desc = request('desc');
            if (request('is_pic_change')) {
                $ext        = getExtentionImages(request('pic'));
                $img_name   = createImageName(uniqid(),$ext[1]);
                $path       = $this->arena_path . $img_name;
                $img        = \Image::make(file_get_contents(request('pic')))->save($path);
                $arena->pic = $img_name;
            }
            $arena->updated_by = JWTAuth::parseToken()->authenticate()->id;
            $arena->save();  // save the arena details


            return response()->json(['message' => 'Arena updated!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteArena($id = null)
    {
        DB::beginTransaction();

        try {

            $arena = Arena::find($id);

            if (!$arena) {
                return response()->json(['message' => 'Could not find arena!'], 422);
            }

            if ($arena->delete()) {
                DB::commit();
                $responseArr = [
                    'message' => 'Arena has been deleted successfully!',
                ];
            } else {
                DB::rollback();
                $responseArr = [
                    'message' => 'Something went wrong!',
                ];
            }

            return response()->json($responseArr);

        } catch (\Exception $ex) {

            DB::rollback();
            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }
}