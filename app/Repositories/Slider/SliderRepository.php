<?php

namespace App\Repositories\Slider;

use App\Repositories\BaseRepository;
use App\Models\Slider\Slider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class SliderRepository extends BaseRepository
{
    
    protected $slider_path = 'images/slider/';
    /**
     * Associated Repository Model.
     */
    const MODEL = Slider::class;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllSlider($request)
    {
        try {

            $slider = Slider::select('sliders.*');

            if ($request->has('title')) {
                $slider->where('title', 'like', '%'.request('title').'%');
            }

            if ($request->has('desc')) {
                $slider->where('desc', 'like', '%'.request('desc').'%');
            }

            $slider->orderBy(request('sortBy'), request('order'));

            return $slider->paginate(request('pageLength'));

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function showSlider($id = null)
    {
        try {

            $slider = Slider::whereId($id)->first();
            $slider->tag = unserialize(base64_decode($slider->tag));
            if (!$slider) {
                return response()->json(['message' => 'Could not find Slider!', 422]);
            }

            return $slider;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json(['message' => $ex->getMessage()], 422);
        }
    }

    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSlider($input = null)
    {
        try {

            $validation = Validator::make($input, [
                'pic_mobile'    => 'required',
                'pic_desktop'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            if (request('pic_mobile')) {
                $ext_mobile        = getExtentionImages(request('pic_mobile'));
                $img_name_mobile   = createImageName(uniqid(),$ext_mobile[1]);
                $path_mobile       = $this->slider_path . $img_name_mobile;
                $img_mobile        = \Image::make(file_get_contents(request('pic_mobile')))->save($path_mobile);
            }

            if (request('pic_desktop')) {
                $ext_desktop        = getExtentionImages(request('pic_desktop'));
                $img_name_desktop   = createImageName(uniqid(),$ext_desktop[1]);
                $path_desktop       = $this->slider_path . $img_name_desktop;
                $img_desktop        = \Image::make(file_get_contents(request('pic_desktop')))->save($path_desktop);
            }

            $slider = Slider::create([
                'title'    => request('title'),
                'desc' => request('desc'),
                'pic_mobile' => $img_name_mobile,
                'pic_desktop' => $img_name_desktop,
                'url' => request('url'),
                'created_by' => JWTAuth::parseToken()->authenticate()->id,
            ]);

            $slider->save();

            return response()->json(['message' => 'Slider added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    public function updateSlider($input = null, $id = null){
        try {

            $validation = Validator::make($input, [
                'pic_mobile'    => 'required',
                'pic_desktop'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $slider = Slider::whereId($id)->first(); // get slider

            $slider->title = request('title');
            $slider->desc = request('desc');
            if (request('is_pic_change')) {
                $ext_mobile        = getExtentionImages(request('pic'));
                $img_name_mobile   = createImageName(uniqid(),$ext_mobile[1]);
                $path_mobile       = $this->slider_path . $img_name_mobile;
                $img_mobile        = \Image::make(file_get_contents(request('pic')))->save($path_mobile);
                $slider->pic_mobile = $img_name_mobile;
            }

            if(request('is_pic_desktop_change')){
                $ext_desktop        = getExtentionImages(request('pic'));
                $img_name_desktop   = createImageName(uniqid(),$ext_desktop[1]);
                $path_desktop       = $this->slider_path . $img_name_desktop;
                $img_desktop        = \Image::make(file_get_contents(request('pic')))->save($path_desktop);
                $slider->pic_desktop = $img_name_desktop;
            }

            $slider->url = request('url');
            $slider->updated_by = JWTAuth::parseToken()->authenticate()->id;
            $slider->save();  // save the slider

            return response()->json(['message' => 'Slider updated!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSlider($id = null)
    {
        DB::beginTransaction();

        try {

            $slider = Slider::find($id);

            if (!$slider) {
                return response()->json(['message' => 'Could not find slider!'], 422);
            }

            if ($slider->delete()) {
                DB::commit();
                $responseArr = [
                    'message' => 'Slider has been deleted successfully!',
                ];
            } else {
                DB::rollback();
                $responseArr = [
                    'message' => 'Something went wrong!',
                ];
            }

            return response()->json($responseArr);

        } catch (\Exception $ex) {

            DB::rollback();
            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }
}