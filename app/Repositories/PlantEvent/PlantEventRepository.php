<?php

namespace App\Repositories\PlantEvent;

use App\Repositories\BaseRepository;
use App\Models\PlantEvent\PlantEvent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use JWTAuth;

class PlantEventRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PlantEvent::class;

    
    /**
     * @param null $input
     * @return \Illuminate\Http\JsonResponse
     */
    public function storePlantEvent($input = null)
    {
        try {

            $validation = Validator::make($input, [
                'name'    => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json(['message' => $validation->messages()->first()], 422);
            }

            $plant_event = PlantEvent::create([
                'arena_id'  => request('arena_id'),
                'name'  => request('name'),
                'organisasi' => request('organisasi'),
                'email' => request('email'),
                'tlp' => request('tlp'),
                'tgl_event' => request('tgl_event'),
                'event_type_id' => request('event_type_id'),
                'jumlah_peserta' => request('jumlah_peserta'),
            ]);

            $plant_event->save();

            return response()->json(['message' => 'PlantEvent added!']);

        } catch (\Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json(['message' => 'Sorry, something went wrong!'], 422);
        }
    }

}