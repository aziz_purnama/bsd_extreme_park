<?php

namespace App\Models\Arena;

use Eloquent;

/**
 * Class Arena.
 */
class Arena extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'arenas';

    protected $fillable = [
        'name',
        'desc',
        'pic',
        'operasional_time',
        'price',
        'created_by',
        'updated_by',
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
