<?php

namespace App\Models\Product;

use Eloquent;

/**
 * Class Product.
 */
class Product extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products';

    protected $fillable = [
        'title',
        'desc',
        'pic',
        'tag',
        'price',
        'discount_price',
        'created_by',
        'updated_by',
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
