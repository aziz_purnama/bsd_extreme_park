<?php

namespace App\Models\Chart;

use Eloquent;

/**
 * Class Chart.
 */
class Chart extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'charts';

    protected $fillable = [
        'product_id',
        'user_id',
        'qty',
        'status',
        'price',
        'total_price',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
