<?php

namespace App\Models\Article;

use Eloquent;

/**
 * Class Article.
 */
class Article extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'articles';

    protected $fillable = [
        'title',
        'desc',
        'pic',
        'tag',
        'category',
        'user_id'
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
