<?php

namespace App\Models\Payment;

use App\Models\Payment\Traits\Relationship\PaymentRelationship;

use Eloquent;

/**
 * Class Payment.
 */
class Payment extends Eloquent
{
    use PaymentRelationship;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'payments';

    protected $fillable = [
        'user_id',
        'transaction_code',
        'transaction_date',
        'total_payment',
        'payment_metod',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
