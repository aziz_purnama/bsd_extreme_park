<?php

namespace App\Models\PaymentDetail;

use Eloquent;

/**
 * Class PaymentDetail.
 */
class PaymentDetail extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'payment_details';

    protected $fillable = [
        'id',
        'payment_id',
        'product_name',
        'product_picture',
        'product_price',
        'product_price_total',
        'qty',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',

    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
