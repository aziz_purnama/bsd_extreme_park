<?php

namespace App\Models\Payment\Traits\Relationship;

use App\Models\Payment\PaymentDetail;

/**
 * Trait UserRelationship.
 */
trait PaymentRelationship
{
    /**
     * One-to-One relationship with payment_detail.
     *
     * @return mixed
     */
    public function payment_detail()
    {
        return $this->hasOne(PaymentDetail::class);
    }
}
