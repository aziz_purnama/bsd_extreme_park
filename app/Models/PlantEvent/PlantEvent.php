<?php

namespace App\Models\PlantEvent;

use Eloquent;

/**
 * Class PlantEvent.
 */
class PlantEvent extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'plant_events';

    protected $fillable = [
        'id',
        'arena_id',
        'name',
        'organisasi',
        'email',
        'tlp',
        'tgl_event',
        'event_type_id',
        'jumlah_peserta',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
