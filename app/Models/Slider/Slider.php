<?php

namespace App\Models\Slider;

use Eloquent;

/**
 * Class Slider.
 */
class Slider extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'sliders';
    
    protected $fillable = [
        'title',
        'desc',
        'pic_mobile',
        'pic_desktop',
        'url',
        'created_by',
        'updated_by',
    ];


     /**
     * @var string
     */
    protected $primaryKey = 'id';
}
